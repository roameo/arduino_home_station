This is a quick and rough implementation using the adafruit PN532 library.

The nfc_card_read should be used for access control in the home station.

The nfc_card_setup sketch should be used to create new cards. All created cards will work with all home station locks.

## NOTE!
The type of cards being used (mifare classic) are known to have security flaws. These types of cards can easily be copied given the attacker has the right hardware.

The current setup does not rely on the UID (except for the length which indicates it is a mifare classic card). The setup sketch will write to one of the blocks on EEPROM as well as change one of the encyption keys for the respective sector from the default value. The written block is what is used to validate keys.

# QUICK START

1. git submodule update --init --recursive
2. Copy "Adafruit-PN532" to the arduino library directory
3. Flash nfc_card_read for use on the home_station. nfc_card_setup for setting up cards.


# Requirements
This only requires the PN532 SHIELD (not breakout.. this requires an additional level shifter) from adafruit. This has been tested only on Arduino Mega R3s but should work with UNO as well.

See "setup" folder for images of setup.
