#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout with SPI, define the pins for SPI communication.
#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

// Uncomment just _one_ line below depending on how your breakout or shield
// is connected to the Arduino:

// Use this line for a breakout with a software SPI connection (recommended):
// Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

// Use this line for a breakout with a hardware SPI connection.  Note that
// the PN532 SCK, MOSI, and MISO pins need to be connected to the Arduino's
// hardware SPI SCK, MOSI, and MISO pins.  On an Arduino Uno these are
// SCK = 13, MOSI = 11, MISO = 12.  The SS line can be any digital IO pin.
//Adafruit_PN532 nfc(PN532_SS);

// Or use this line for a breakout or shield with an I2C connection:
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

#define DEFAULT_KEY_A {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
#define DEFAULT_BLOCK_7 {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x07, 0x80, 0x69, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
#define RAD_SECTOR_1_KEY_A {0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F}
#define RAD_BLOCK_4 {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}

#define RED_LED		13
#define GREEN_LED	12
#define YELLOW_LED	11


void print_data_block(uint8_t block)
{
	uint8_t data[16];
	uint8_t success;

	success = nfc.mifareclassic_ReadDataBlock(block, data);
	if (success)
	{
		// Data seems to have been read ... spit it out
		Serial.print("Contents: ");
		nfc.PrintHexChar(data, 16);
	}
	else
	{
		Serial.println("Unable to read the requested block.");
	}
}

void write_data_block(uint8_t block, uint8_t data[])
{
	uint8_t success;

	success = nfc.mifareclassic_WriteDataBlock (block, data);
	if (success)
	{
		Serial.println("Successfully wrote to block");
	}
	else
	{
		Serial.println("Unable to write the requested block.");
	}
}

bool check_card_correct()
{
	bool overall_success = false;
	bool success = false;

	uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };
	uint8_t uidLength;
	
	success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 500);
	if (success && uidLength == 4)
	{
		uint8_t keya[6] = RAD_SECTOR_1_KEY_A;
		success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4, 0, keya);
	
		if (success)
		{
			uint8_t data[16];

			success = nfc.mifareclassic_ReadDataBlock(4, data);
			if (success)
			{
				uint8_t rad_block_4[16] = RAD_BLOCK_4;
				if (memcmp(data, rad_block_4, sizeof(data)) == 0)
				{
					overall_success = true;
					Serial.println("CARD TEST PASSED");
				}
			}
		}
	}

	return overall_success;
}


void setup(void) {
	pinMode(RED_LED, OUTPUT);
	pinMode(GREEN_LED, OUTPUT);
	pinMode(YELLOW_LED, OUTPUT);
	digitalWrite(RED_LED, LOW);
	digitalWrite(GREEN_LED, LOW);
	digitalWrite(YELLOW_LED, LOW);

	Serial.begin(115200);
	while (!Serial) delay(10); // for Leonardo/Micro/Zero

	Serial.println("Card Setup");

	nfc.begin();

	uint32_t versiondata = nfc.getFirmwareVersion();
	if (! versiondata) {
		Serial.print("Didn't find PN53x board");
		while (1); // halt
	}
	// Got ok data, print it out!
	Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
	Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
	Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
	
	// configure board to read RFID tags
	nfc.SAMConfig();
	
	Serial.println("Waiting for an ISO14443A Card ...");
}


void loop(void) {
	uint8_t success;
	uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };	// Buffer to store the returned UID
	uint8_t uidLength;							// Length of the UID (4 or 7 bytes depending on ISO14443A card type)
		
	// Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
	// 'uid' will be populated with the UID, and uidLength will indicate
	// if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
	success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
	
	if (success) {
		digitalWrite(YELLOW_LED, HIGH);

		// Display some basic information about the card
		Serial.println("=======================================================================");
		Serial.println("Found an ISO14443A card");
		Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
		Serial.print("  UID Value: ");
		nfc.PrintHex(uid, uidLength);
		Serial.println("");
		
		// Check if (likely) mifare classic card
		if (uidLength == 4)
		{
			
			uint8_t keya[6] = DEFAULT_KEY_A;
			success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4, 0, keya);
		
			if (success)
			{
				Serial.println("Sector 1 (Blocks 4..7) has been authenticated");
				Serial.println("---------------------------------------------");
				uint8_t data[16];

				Serial.println("Re-writing block 4");
				Serial.println("-------------------");
				// Read block 4 current contents 
				print_data_block(4);
				// Write new block 4
				memcpy(data, (const uint8_t[])RAD_BLOCK_4, sizeof data);
				write_data_block(4, data);
				// Read block 4 new contents 
				print_data_block(4);
		
				Serial.println("");

				Serial.println("Re-writing block 7");
				Serial.println("-------------------");
				// Read block 7 current contents 
				print_data_block(7);
				// Write new block 7
				memcpy(data, (const uint8_t[])DEFAULT_BLOCK_7, sizeof data);
				memcpy(&data[0], (const uint8_t[])RAD_SECTOR_1_KEY_A, 6);
				write_data_block(7, data);
				// Read block 7 new contents 
				print_data_block(7);

			}
			else
			{
				Serial.println("Ooops ... authentication failed: Try another key?");
			}
		}

		if (check_card_correct())
		{
			digitalWrite(GREEN_LED, HIGH);
		}
		else
		{
			digitalWrite(RED_LED, HIGH);
		}

		delay(2000);
	}

	digitalWrite(GREEN_LED, LOW);
	digitalWrite(RED_LED, LOW);
	digitalWrite(YELLOW_LED, LOW);
}

